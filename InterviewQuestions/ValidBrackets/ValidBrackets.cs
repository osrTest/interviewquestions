﻿using System;

namespace InterviewQuestions
{
    public class ValidBrackets
    {
        // TODO: Implement this method in the most efficient way you can.
        /// <summary>
        /// Gets a string and returns true if the string's brackets are valid or not.
        /// Valid input: abc(def[ghi]jkl{mno}pqr<stu>vwx)yz
        /// Not valid input: abc(def[ghi)jkl{mno}pqr<stu>vwx]yz
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool IsStringValid(string text)
        {
            throw new NotImplementedException();
        }
    }
}
