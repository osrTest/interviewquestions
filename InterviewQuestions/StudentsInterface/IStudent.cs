﻿namespace InterviewQuestions
{
    public interface IStudent
    {
        /// <summary>
        /// Gets an index in the array and returns the grade for the student in that index.
        /// </summary>
        /// <param name="index">Array index</param>
        /// <returns></returns>
        int GetGrade(int index);

        /// <summary>
        /// Gets an index in the array and sets the students grade to the given grade in that index.
        /// </summary>
        /// <param name="index">Array index</param>
        /// <param name="grade">grade to set for the student</param>
        void SetGrade(int index, int grade);

        /// <summary>
        /// Gets a grade that should be returned to all students after callnig this method.
        /// </summary>
        /// <param name="grade">grade to be set</param>
        void SetAllGrades(int grade);
    }
}
