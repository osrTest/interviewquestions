﻿namespace InterviewQuestions
{
    public class Student
    {
        public string Name;
        public int Grade;

        public Student(string name, int grade)
        {
            Name = name;
            Grade = grade;
        }
    }
}
