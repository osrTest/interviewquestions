﻿using System;

namespace InterviewQuestions
{
    // TODO: Implement the IStudent interface in the most efficient way you can.
    public class Students : IStudent
    {
        Student[] students;

        public void Init(int n)
        {
            students = new Student[n];
            for (int i = 0; i < n; i++)
                students[i] = new Student($"Student_{i}", i);
        }

        /// <summary>
        /// Gets an index in the array and sets the students grade to the given grade in that index.
        /// </summary>
        /// <param name="index">Array index</param>
        /// <param name="grade">grade to set for the student</param>
        public int GetGrade(int index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets an index in the array and sets the students grade to the given grade in that index.
        /// </summary>
        /// <param name="index">Array index</param>
        /// <param name="grade">grade to set for the student</param>
        public void SetGrade(int index, int grade)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a grade that should be returned to all students after callnig this method.
        /// </summary>
        /// <param name="grade">grade to be set</param>
        public void SetAllGrades(int grade)
        {
            throw new NotImplementedException();
        }
    }
}
