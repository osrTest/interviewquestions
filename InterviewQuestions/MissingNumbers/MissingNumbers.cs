﻿using System;

namespace InterviewQuestions
{
    public class MissingNumbers
    {
        int[] numbers;

        public void Init(int n)
        {
            numbers = new int[n];
            for (int i = 0; i < n; i++)
                numbers[i] = i + 1;

            Random rand = new Random();

            int random1 = rand.Next(1, n);
            int random2 = rand.Next(1, n);

            numbers[random1] = 0;
            numbers[random2] = 0;
        }

        /// <summary>
        /// Sets the numbers at the given indexes to 0.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public (int first, int second) PullNumbers(int firstIndex, int secondIndex)
        {
            (int first, int second) = (numbers[firstIndex], numbers[secondIndex]);
            numbers[firstIndex] = 0;
            numbers[secondIndex] = 0;

            return (first, second);
        }

        // TODO: Find the 2 numbers that were "pulled out" from the array.
        // Implement this method in the most efficient way you can.
        public (int first, int second) FindMissingNumbers()
        {
            throw new NotImplementedException();
        }
    }
}
