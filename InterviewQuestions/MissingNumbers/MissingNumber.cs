﻿using System;

namespace InterviewQuestions
{
    public class MissingNumber
    {
        int[] numbers;

        public void Init(int n)
        {
            numbers = new int[n];
            for (int i = 0; i < n; i++)
                numbers[i] = i + 1;
        }

        /// <summary>
        /// Sets the number at the given index to 0.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int PullNumber(int index)
        {
            int pulledNumber = numbers[index];
            numbers[index] = 0;

            return pulledNumber;
        }

        // TODO: Find the number that was "pulled out" from the array.
        // Implement this method in the most efficient way you can.
        public int FindMissingNumber()
        {
            throw new NotImplementedException();
        }
    }
}
