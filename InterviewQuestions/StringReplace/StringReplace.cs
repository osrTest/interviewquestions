﻿using System;

namespace InterviewQuestions
{
    public class StringReplace
    {
        // TODO: Implement the following method according to the specs given bellow.
        /// <summary>
        /// Returns a new string in which all occurrences of a specified Unicode character
        /// in this instance are replaced with another specified Unicode character.
        /// For example: Replace("abcdefg", "cd", "acd")
        /// shall return: ababcefg
        /// </summary>
        /// <param name="input"></param>
        /// <param name="oldString"></param>
        /// <param name="newString"></param>
        public string Replace(string input, string oldString, string newString)
        {
            throw new NotImplementedException();
        }
    }
}
