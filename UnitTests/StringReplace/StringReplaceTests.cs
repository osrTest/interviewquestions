﻿using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class StringReplaceTests
    {
        readonly StringReplace test = new StringReplace();

        [TestMethod]
        public void TestReplace()
        {
            string text = test.Replace("abcdef", "cd", "abc");
            Assert.AreEqual("ababcef", text);
        }
    }
}
