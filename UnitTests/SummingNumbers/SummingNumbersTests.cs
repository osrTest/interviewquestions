﻿using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class SummingNumbersTests
    {
        readonly SummingNumbers test = new SummingNumbers();

        [TestMethod]
        public void TestHasTwoSummingNumbers()
        {
            int[] input = new int[] { 1, 15, 10, 8, 5, 2 };

            bool shouldBeTrue = test.HasTwoSummingNumbers(input, 13);
            bool shouldBeFalse = test.HasTwoSummingNumbers(input, 19);

            Assert.IsTrue(shouldBeTrue);
            Assert.IsFalse(shouldBeFalse);
        }
    }
}
