﻿using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class SummingNumbersSortedTests
    {
        readonly SummingNumbersSorted test = new SummingNumbersSorted();

        [TestMethod]
        public void TestHasTwoSummingNumbers()
        {
            int[] input = new int[] { 1, 2, 5, 8, 10, 15 };

            bool shouldBeTrue = test.HasTwoSummingNumbers(input, 13);
            bool shouldBeFalse = test.HasTwoSummingNumbers(input, 19);

            Assert.IsTrue(shouldBeTrue);
            Assert.IsFalse(shouldBeFalse);
        }
    }
}
