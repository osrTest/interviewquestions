﻿using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class StudentsInterfaceTests
    {
        Students test = new Students();

        [TestMethod]
        public void TestInterfaceImplementation()
        {
            const int size = 10;

            Random rand = new Random();
            int index = rand.Next(size);

            test.Init(size);
            
            Assert.AreEqual(index, test.GetGrade(index));

            test.SetGrade(index, 15);
            Assert.AreEqual(15, test.GetGrade(index));

            test.SetAllGrades(20);
            Assert.AreEqual(20, test.GetGrade(rand.Next(size)));

            test.SetGrade(index, 25);
            Assert.AreEqual(25, test.GetGrade(index));

            test.SetAllGrades(30);
            Assert.AreEqual(30, test.GetGrade(rand.Next(size)));

            test.SetGrade(index, 35);
            Assert.AreEqual(35, test.GetGrade(index));
        }
    }
}
