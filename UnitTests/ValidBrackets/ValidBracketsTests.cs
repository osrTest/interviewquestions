﻿using System;
using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class ValidBracketsTests
    {
        readonly ValidBrackets test = new ValidBrackets();

        [TestMethod]
        public void TestIsStringValid()
        {
            bool shouldBeTrue = test.IsStringValid("{()(n[j])skd{;[nj(kh)cg]jh}nfc<;d>j;v(nj)kn[hq];n{ch}fjewk((((f{.j}dk))))nr;jfbnv({[<vbhhkfv>]})bueytbcf}");
            bool shouldBeFalse = test.IsStringValid("{()(n[j])skd{;[nj(kh)cg]jh}nfc<;d>j;v(nj)kn[hq];>(n{ch}fjewk((((f{.j}dk))))nr;jfbnv({[<vbhhkfv>]})bueytbcf}");

            Assert.IsTrue(shouldBeTrue);
            Assert.IsFalse(shouldBeFalse);
        }
    }
}
