﻿using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class MissingNumberTests
    {
        readonly MissingNumber test = new MissingNumber();

        [TestMethod]
        public void TestFindMissingNumber()
        {
            test.Init(100);
            int pulledNumber = test.PullNumber(new Random().Next(0, 99));

            Assert.AreEqual(pulledNumber, test.FindMissingNumber());
        }
    }
}
