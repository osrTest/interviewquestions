﻿using InterviewQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class MissingNumbersTests
    {
        readonly MissingNumbers test = new MissingNumbers();

        [TestMethod]
        public void TestFindMissingNumber()
        {
            test.Init(100);

            Random rand = new Random();
            (int firstPulled, int secondPulled) = test.PullNumbers(rand.Next(0, 99), rand.Next(0, 99));

            (int firstFound, int secondFound) = test.FindMissingNumbers();

            bool firstNumberFound = (firstFound == firstPulled) || (firstFound == secondPulled);
            bool secondNumberFound = (secondFound == firstPulled) || (secondFound == secondPulled);

            Assert.IsTrue(firstNumberFound);
            Assert.IsTrue(secondNumberFound);
        }
    }
}
